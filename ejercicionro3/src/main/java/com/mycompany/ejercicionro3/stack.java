
package com.mycompany.ejercicionro3;

/**
 *
 * @author Jherson Rodrigo Mamani Poma
 */
public class stack {
    int size; 
    int arr[];
    int top;
    
    stack(int size){
        this.size = size;
        this.arr = new int[size];
        this.top = -1;
    }
    
    public boolean isEmpty(){ 
        return (top == -1);
    }
    
    public boolean isFull(){
        return (size-1 == top);
    }
    
    public void push(int element){
        if(!isFull()){
            top++;
            arr[top] = element;
        }else{
            System.out.println("La pila se encuentra llena");
        }
    }
    public int peek(){
        return arr[top];
    }
    
    public int pop(){
        if(!isEmpty()){
            int returntop = top;
            top--;
            int temp = arr[returntop];
            return temp;
        }else{
            return -1;
        }
    }    
}
