
package com.mycompany.ejercicionro3;
import javax.swing.JOptionPane;
/**
 *
 * @author Jherson Rodrigo Mamani Poma
 * Realizar un programa que pida números por pantalla y sean almacenados en una pila (estática o dinámica) y crear una función para que los números se muestren una sola vez el
 * resultado final debe ser ordenado de manera descendente pueden usar pilas auxiliares si es necesario
 * 1.- ENTRADA 7,3,4,4,6,7,8 SALIDA 8,7,6,4,3
  -El programa debe solicitar que tipo de pila se desea utilizar para el almacenamiento
  -Las pilas deben ser creadas por usted y no así el uso de la clase stack de java. -Los resultados deben ser mostrados por pantalla como lo vamos haciendo en clases
  -RESPETAR LA ESTRUCTURA DE DATOS DE LA PILA EL PRIMERO EN ENTRAR DEBE SER EL ULTIMO EN SALIR Y EL ULTIMO EN ENTRAR DEBE SER EL PRIMER EN SALIR
 */
public class main {
    public static void main(String[] args){
        stack p = new stack(7);
        
        String inputtext;
        do{
            inputtext = JOptionPane.showInputDialog("Introdusca un entero");
            if(inputtext.length() == 0){
                inputtext = JOptionPane.showInputDialog("Introdusca un entero");
            }else{
                p.push(Integer.parseInt(inputtext));
            }
        }while(!p.isFull());

        stack tp = new stack(7);
        stack tp2 = new stack(7);
        while(!p.isEmpty()){
            int temp = p.pop();
            tp2.push(temp);
            while(!p.isEmpty()){
                int temp2 = p.pop();
                if(temp2 != temp){
                    tp.push(temp2);
                }
            }
            while(!tp.isEmpty()){
                p.push(tp.pop());
            }
        }
        
        String result = "";
        while(!tp2.isEmpty()){
            result = result + " - " + tp2.pop();
        }
        
        JOptionPane.showMessageDialog(null,"el resultado es \n"+result);
                
    }
}
