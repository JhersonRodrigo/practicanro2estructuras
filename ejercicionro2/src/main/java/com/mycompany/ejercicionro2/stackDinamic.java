
package com.mycompany.ejercicionro2;
import java.util.ArrayList;
/**
 *
 * @author Jherson Rodrigo Mamani Poma
 */
public class stackDinamic {
    private ArrayList<Integer> stack = new ArrayList();
    
    public Boolean isEmpty(){
        return stack.isEmpty();
    }
    
    public void push(int o){
        stack.add(o);
    }
    
    public int peek(){
        if(!(stack.isEmpty())){
            return stack.get(stack.size()-1);
        }else{
            return -1;
        }
    }
    
    public int pop(){
        if(!(stack.isEmpty())){
            int o = stack.get(stack.size()-1);
            stack.remove(stack.size()-1);
            return o;
        }else{
            return -1;
        }
    }
}
