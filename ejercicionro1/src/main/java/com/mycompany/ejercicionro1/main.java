
package com.mycompany.ejercicionro1;
import javax.swing.JOptionPane;
/**
 *
 * @author Jherson Rodrigo Mamani Poma
 * Realizar un programa que pida números por pantalla y sean almacenados en una pila (estática o dinámica) y crear una función para que la pila sea ordenada de manera
 * ascendente y descendente pueden usar pilas auxiliares si es necesario
 * 1.- ENTRADA 7,3,5,4,6,7,8 SALIDA 3,4,5,6,7,7,8
 * 2.- ENTRADA 7,3,5,4,6,7,8 SALIDA 8,7,7,6,5,4,3
  -El programa debe solicitar que tipo de pila se desea utilizar para el almacenamiento
  -Las pilas deben ser creadas por usted y no así el uso de la clase stack de java. -Los resultados deben ser mostrados por pantalla como lo vamos haciendo en clases
  -RESPETAR LA ESTRUCTURA DE DATOS DE LA PILA EL PRIMERO EN ENTRAR DEBE SER EL ULTIMO EN SALIR Y EL ULTIMO EN ENTRAR DEBE SER EL PRIMER EN SALIR
 */
public class main {
     public static void main(String[] args){
        String inputText,inputSelect,inputData,inputType;
        inputText = JOptionPane.showInputDialog("Introdusca la cantidad de datos que desea ingresar");
        inputSelect = JOptionPane.showInputDialog("Que tipo de pila desea utilizar: \n -dinamica \n -estatica");
        int count = 1;
        int limit = Integer.parseInt(inputText);
        String result = "";
        switch(inputSelect){
            case "dinamica":
                stackDinamic pd = new stackDinamic();
                do{
                    inputData = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputData.length() == 0){
                        inputData = JOptionPane.showInputDialog("Introduzca un numero entero");    
                    }else{
                        pd.push(Integer.parseInt(inputData));
                    }
                    count++;
                }while(count <= limit);
                inputType = JOptionPane.showInputDialog("Ordenar: \n -ascendente \n -descendente");
                stackDinamic resultStackDinamic = sortStackDinamic(pd,inputType);
                while(!resultStackDinamic.isEmpty()){
                    result = result+ " - " + resultStackDinamic.pop();
                }
                JOptionPane.showMessageDialog(null, result);
            break;
            case "estatica":
                stackStatic ps = new stackStatic(limit);
                do{
                    inputData = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputData.length() == 0){
                        inputData = JOptionPane.showInputDialog("Introduzca un numero entero");    
                    }else{
                        ps.push(Integer.parseInt(inputData));
                    }
                    count++;
                }while(count <= limit);
                inputType = JOptionPane.showInputDialog("ordenar: \n -ascendente \n -descendente");
                stackStatic resultStackStatic = sortStackStatic(ps,inputType);
                while(!resultStackStatic.isEmpty()){
                    result = result+ " - " + resultStackStatic.pop();
                }   
                JOptionPane.showMessageDialog(null, result);
            break;
            default:
                JOptionPane.showMessageDialog(null, "Elija una opcion valida");
            break;
        }
    }
    
    public static stackStatic sortStackStatic(stackStatic p,String type){
        stackStatic tp = new stackStatic(10);
        while(!p.isEmpty()){
            int currentData = p.pop();
            if(type == "ascendente"){
                while(!tp.isEmpty() && tp.peek() > currentData){
                    p.push(tp.pop());
                }
                tp.push(currentData); 
            }else{
                while(!tp.isEmpty() && tp.peek() < currentData){
                    p.push(tp.pop());
                }
                tp.push(currentData);       
            }
        }
        return tp;
    }
    
    public static stackDinamic sortStackDinamic(stackDinamic p,String type){
        stackDinamic tp = new stackDinamic();
        while(!p.isEmpty()){
            int currentData = p.pop();
            while(!tp.isEmpty() && tp.peek() > currentData){
                p.push(tp.pop());
            }
            tp.push(currentData);
        }
        return tp;
    }
}
