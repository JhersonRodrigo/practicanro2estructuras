
package com.mycompany.ejercicionro6;

/**
 *
 * @author Jherson Rodrigo MamanI Poma
 */
public class process {
   String mycad;
    stackDinamic stackval = new stackDinamic();
    
    public process(String str){
        this.mycad = str;
    }
    
    public void checkStackVal(String strs){   
        for(int i = 0;i<strs.length();i++ ){
            if(strs.charAt(i) == '('){
                stackval.push(')');
            }else if(strs.charAt(i) == '['){
                stackval.push(']');
            }else if(strs.charAt(i) == ']'){
                checkStack(']','[');
            }else if(strs.charAt(i) == ')'){
                checkStack(')','(');
            }
        }
        
        if(stackval.isEmpty()){
            System.out.println("Balanceado");
        }else{
            System.out.println("No balanceado");
        }
    }
   
    public void checkStack(char c, char cc){
        if(stackval.isEmpty()){
            System.out.println("No balanceado" + cc);
            System.exit(0);
        }else{
            char s = stackval.pop();
            if(c != s){
                System.out.println("No balanceado" + s);    
                System.exit(0);
            }
        }
    } 
}
