
package com.mycompany.ejercicionro6;
import javax.swing.JOptionPane;
/**
 *
 * @author Jherson Rodrigo Mamani Poma
 * Realizar un programa que Lea una cadena y determine si los símbolos ( ) y [ ] están correctamente balanceados. Si no lo están muestre el error indicando el símbolo faltante. Use una pila para cada símbolo. -Las pilas deben ser creadas por usted y no así el uso de la clase stack de java. -cree una función para verificar la cadena
-Los resultados deben ser mostrados por pantalla como lo vamos haciendo en clases
-RESPETAR LA ESTRUCTURA DE DATOS DE LA PILA EL PRIMERO EN ENTRAR DEBE SER EL ULTIMO EN SALIR Y EL ULTIMO EN ENTRAR DEBE SER EL PRIMER EN SALIR
 */
public class main {
    public static void main(String[] args){
        process  p = new process("x");
        p.checkStackVal("[(hola)]");
    }
}

